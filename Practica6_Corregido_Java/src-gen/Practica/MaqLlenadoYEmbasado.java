package Practica;
import java.time.LocalDate;
import java.util.ArrayList;

public class MaqLlenadoYEmbasado extends Maquina {
	private int cantidadEnvases;
	private ArrayList listaRegulacion;
	
	public float costoLlenadoYEnvasado() {
		float costoLlenadoYEnvasado=0;
		costoLlenadoYEnvasado=(float) (this.getCosto()*0.0025/this.cantidadEnvases);
		return costoLlenadoYEnvasado;
	}
	public MaqLlenadoYEmbasado(String descripcion, LocalDate fechaAdquisicion, float costo, int cantidadEnvases, ArrayList listaRegulacion) {
		super(descripcion, fechaAdquisicion, costo);
		this.cantidadEnvases = cantidadEnvases;
		this.listaRegulacion = listaRegulacion;
	}
	@Override
	public String toString() {
		return "MaqLlenadoYEmbasado [cantidadEnvases=" + cantidadEnvases + ", listaRegulacion=" + listaRegulacion + ", toString()=" + super.toString() + "]";
	}
	public int getCantidadEnvases() {
		return cantidadEnvases;
	}
	public void setCantidadEnvases(int cantidadEnvases) {
		this.cantidadEnvases = cantidadEnvases;
	}
	public ArrayList getListaRegulacion() {
		return listaRegulacion;
	}
	public void setListaRegulacion(ArrayList listaRegulacion) {
		this.listaRegulacion = listaRegulacion;
	}
}