package Practica;
import java.time.LocalDate;

public class MaquinaEmpaquetado extends Maquina {
	private String tipoDeEmpaque;
	private int cantidadEmpaquesXMin;
	
	public float costoDeEmpaquetado() {
		float costoDeEmpaquetado=0;
		costoDeEmpaquetado=(float) (this.getCosto()*0.0006/this.cantidadEmpaquesXMin);
		return costoDeEmpaquetado;
	}
	public MaquinaEmpaquetado(String descripcion, LocalDate fechaAdquisicion, float costo, String tipoDeEmpaque, int cantidadEmpaquesXMin) {
		super(descripcion, fechaAdquisicion, costo);
		this.tipoDeEmpaque = tipoDeEmpaque;
		this.cantidadEmpaquesXMin = cantidadEmpaquesXMin;
	}
	@Override
	public String toString() {
		return "MaquinaEmpaquetado [tipoDeEmpaque=" + tipoDeEmpaque + ", cantidadEmpaquesXMin=" + cantidadEmpaquesXMin + ", toString()=" + super.toString() + "]";
	}
	public String getTipoDeEmpaque() {
		return tipoDeEmpaque;
	}
	public void setTipoDeEmpaque(String tipoDeEmpaque) {
		this.tipoDeEmpaque = tipoDeEmpaque;
	}
	public int getCantidadEmpaquesXMin() {
		return cantidadEmpaquesXMin;
	}
	public void setCantidadEmpaquesXMin(int cantidadEmpaquesXMin) {
		this.cantidadEmpaquesXMin = cantidadEmpaquesXMin;
	}
}