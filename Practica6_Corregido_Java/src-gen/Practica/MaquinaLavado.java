package Practica;
import java.time.LocalDate;

public class MaquinaLavado extends Maquina{
	private float capacidadLitros;
	private float tiempoCicloXBotella;
	
	public float costoLavadoBotella() {
		float costoLavadoBotella=0;
		costoLavadoBotella=(float) (this.getCosto()*0.005/this.tiempoCicloXBotella);
		return costoLavadoBotella;
	}
	public MaquinaLavado(String descripcion, LocalDate fechaAdquisicion, float costo, float capacidadLitros, float tiempoCicloXBotella) {
		super(descripcion, fechaAdquisicion, costo);
		this.capacidadLitros = capacidadLitros;
		this.tiempoCicloXBotella = tiempoCicloXBotella;
	}
	@Override
	public String toString() {
		return "MaquinaLavado [capacidadLitros=" + capacidadLitros + ", tiempoCicloXBotella=" + tiempoCicloXBotella + ", toString()=" + super.toString() + "]";
	}
	public float getCapacidadLitros() {
		return capacidadLitros;
	}
	public void setCapacidadLitros(float capacidadLitros) {
		this.capacidadLitros = capacidadLitros;
	}
	public float getTiempoCicloXBotella() {
		return tiempoCicloXBotella;
	}
	public void setTiempoCicloXBotella(float tiempoCicloXBotella) {
		this.tiempoCicloXBotella = tiempoCicloXBotella;
	}
}