package Practica;
import java.time.LocalDate;

public class Maquina {
	private String descripcion;
	private LocalDate fechaAdquisicion;
    private float costo;
    
    public Maquina(String descripcion, LocalDate fechaAdquisicion, float costo) {
    	super();
    	this.descripcion = descripcion;
    	this.fechaAdquisicion = fechaAdquisicion;
    	this.costo = costo;
    }
	@Override
	public String toString() {
		return "Maquina [descripcion=" + descripcion + ", fechaAdquisicion=" + fechaAdquisicion + ", costo=" + costo + "]";
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public LocalDate getFechaAdquisicion() {
		return fechaAdquisicion;
	}
	public void setFechaAdquisicion(LocalDate fechaAdquisicion) {
		this.fechaAdquisicion = fechaAdquisicion;
	}
	public float getCosto() {
		return costo;
	}
	public void setCosto(float costo) {
		this.costo = costo;
	}  
}