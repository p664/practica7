package ito.poo.app;
import java.time.LocalDate;
import Practica.MaqLlenadoYEmbasado;
import Practica.Maquina;
import Practica.MaquinaEmpaquetado;
import Practica.MaquinaLavado;

public class MyApp {

	static void run() {
		MaquinaLavado c=new MaquinaLavado("Maquina de Lavado", LocalDate.of(2021, 10, 17), 18000f, 3f, 12f);
		System.out.println(c);
		System.out.println(c.costoLavadoBotella());
		
		MaqLlenadoYEmbasado ct=new MaqLlenadoYEmbasado("Maquina de Llenado y Embasado", LocalDate.of(2021, 10, 28), 21000f, 5, null);
		System.out.println(ct);
		System.out.println(ct.costoLlenadoYEnvasado());

		MaquinaEmpaquetado costo=new MaquinaEmpaquetado("Maquina de Empaquetado", LocalDate.of(2021, 10, 31), 8000f, "cart�n de 9", 2);
		System.out.println(costo);
		System.out.println(costo.costoDeEmpaquetado());
	}
	
	public static void main(String[] args) {
		run();
	}
}
